/*O programa não aceita valores negativos ou flutuantes. apenas inteiros positivos  ou iguais a zero*/

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

// Declaração da função no header
long int convert_base(int num1, int num2, long int num3, long int base);

char valid_ans(void);

int main(void)
{
    char ans;
    do
    {
        // VAriaveis
        int num, base, nDigits, count = 0;
        // Entrada do numero a ser convertido
        printf("Digite o numero que deseja converter(apenas valores inteiros): ");
        scanf("%d", &num);

        do
        {
            printf("Selecione uma opcao para realizar a conversao:\n1 - Binario\n2 - Octal\n3 - Hexadecimal\nOpcao: ");
            scanf("%d", &base);

            if (base < 1 || base > 3)
            {
                // O usuario terá 3 tentativas antes do programa fechar,
                // se errar o valor as 3 vezes, devera reiniciar o programa
                printf("OPCAO INVALIDA! TENTATIVAS RESTANTES: %d\n\n", 3 - count);
                count++;

                if (count == 4)
                    return 1;
            }
        }
        while (base < 1 || base > 3);

        if (num == 1 || num == 0)
        {
            if (base != 3)
                printf("\nO novo numero eh: %d", num);

            // Incluindo a notação em hexadecimal
            else
                printf("\nO novo numero eh: 0x%d", num);

            return 0;
        }
        // Converte a base para valores que usarei na função convert_base()
        switch (base)
        {
            case 1:
            base = 2;
            break;

            case 2:
            base = 8;
            break;

            case 3:
            base = 16;
            break;
        }
        // Calcula o numero de digitos necessários com base nas potencias
        int k = 1;
        while (true)
        {
            if (pow(base, k) <= num)
                k++;
            else
                break;
        }

        printf("\n\nO novo numero eh: ");
        // Prefixo 0x caso a a conversao seja para hexadecimal
        if (base == 16)
            printf("0x");

        for (long int i = k - 1; i >= 0; i--)
        {
            // No caso do hexadecimal, teremos a base igual a char ao inves de int
            if (base == 16)
                printf("%c", (char)convert_base(num, k, i, base));

            else
                printf("%ld", convert_base(num, k, i, base));
        }
        ans = valid_ans();
    }
    while(ans == 's' || ans == 'S');
}








// num1 = num, num2 = casas necessarias, num3 = valor atual, base = base desejada
long int convert_base(int num1, int num2, long int num3, long int base)
{
    double resto;
    long int convert[num2];
    char hexChar;
    // Algoritmo de conversão para binário
    for (int i = 0; i < num2; i++)
    {
        // Calcula o resto sem operador de resto
        resto = (double)num1 / base;
        resto -= (long int)resto;
        resto *= base;

        // Dependendo da base selecionada, a função utilizara um dos cases abaixo
        switch (base)
        {
            case 2:
            if (resto == 1)
            convert[i] = 1;

            else
            convert[i] = 0;
            num1 = (num1 - resto) / 2;

            break;

            case 8:
            convert[i] = resto;
            num1 = (num1 - resto) / 8;
            break;

            case 16:
            convert[i] = resto;
            num1 = (num1 - resto) / 16;
            break;
        }
    }
    // No caso dos hexadecimais, será necessário a introdução de letras
    // Usei a tabela ASCII para fazer as converções
    switch (base)
    {
        case 16:
        if (convert[num3] > 9)
        {
            return convert[num3] + 55;
        }

        else
        {
            return convert[num3] + 48;

        }
        break;

        default:
            return convert[num3];
    }
}

// Valida respostas
char valid_ans(void)
{
    char abc;

    do
    {
        printf("\n\nDeseja repetir o programa(s/n): ");
        setbuf(stdin, NULL);
        scanf("%c", &abc);
        abc = toupper(abc);

        if (abc == 'S')
            printf("\n");

        if (abc != 'N' && abc != 'S')
        {
            printf("\nValor invalido, digite S ou N...");
        }
    }
    while (abc != 'N' && abc != 'S');

    return abc;
}
