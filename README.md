# Conversão de bases

O programa em questão tem o objetivo de converter numerais decimais, inteiros e positivos, para outras bases, incluindo binário, octal e hexadecimal. Este programa foi sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O código faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#autores).

A lógica por traz do funcionamento do programa vem do algoritmo segundo a imagem abaixo.

![](https://calculareconverter.com.br/wp-content/uploads/2018/05/decimal-em-bin%C3%A1rio.png)

Basicamente, o programa realiza divisões sucessivas pela base(2 para binario, 8 para octal e 16 para hexadecimal) guardando os restos. Ao final, o numero resultante será dado pelo inverso dos restos recebidos. No caso do haxadeciamis, quando o resto for entre 10 e 15, o valor devolvido será entre A e F.

### Observações

O programa se limita a um numero máximo igual a 2147483647, que seria $`(2^9)^3 * 2^4 - 1`$. Acima disso o programa não funcionará como previsto.

Os numerais hexadecimais terão um sufixo 0x para diferenciar do decimal, uma vez que podem ser confundidos, por exemplo, 11 em hecadicimal vale 17 e não 11(decimal). nesse caso, vc obteria um resultado igual a 0x11. Ja 11 em hexadecimal seria 0xB.

## Execução
caso deseje executar o programa na sua máquina, siga os passos a seguir:

1. Instale o app [Code::Blocks - Baixe a versão codeblocks-20.03mingw-setup.exeURL](https://www.codeblocks.org/downloads/binaries/)
    1. Você também pode usar outro compilador em C, como o visual studio
1. Copie o codigo ou baixe esse [arquivo](https://drive.google.com/file/d/1XG4E6vIrLMGsbPDWFaPCalyFIcGNl38D/view?usp=sharing)
1. Execute o programa! :D



## Autores
Esse programa foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8313095/avatar.png?width=400)  | Satil Pereira | satil_pereira | [satilpereira@utfpr.edu.br](mailto:satilpereira@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8399004/avatar.png?width=400)  | Carlos Ribas | carlos.2003 | [carlos.2003@utfpr.edu.br](mailto:carlos.2003@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8623848/avatar.png?width=400)  | Maria Clara de Lima | Lclara | [mariaclaralima@utfpr.edu.br](mailto:mariaclaralima@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8333209/avatar.png?width=400)  | Rafael Inácio | rafaelinacio829 | [rafaelinacio@alunos.utfpr.edu.br](mailto:rafaelinacio@alunos.utfpr.edu.br)
